package com.example.financeapp.ui.screens.newsScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.financeapp.util.FinanceAppServerClient
import com.example.financeapp.util.News
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class NewsScreenViewModel: ViewModel() {
    private val _news = MutableStateFlow<List<News>>(emptyList())
    val news = _news.asStateFlow()

    private val _popup = MutableStateFlow<News?>(null)
    val popup = _popup.asStateFlow()

    init {
        viewModelScope.launch {
            getNews()
        }
    }

    fun showNews(news: News) {
        _popup.tryEmit(news)
    }

    fun hideNews() {
        _popup.tryEmit(null)
    }

    private suspend fun getNews() {
        val n = FinanceAppServerClient.create().getNews()
        if(n.isSuccessful) {
            if(n.body() != null) {
                _news.emit(n.body()!!)
            }
        }
    }
}