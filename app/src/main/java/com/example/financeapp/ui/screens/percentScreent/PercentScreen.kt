package com.example.financeapp.ui.screens.percentScreent

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.financeapp.R
import com.example.financeapp.ui.composables.*
import com.example.financeapp.ui.theme.Transparent_grey

@Composable
fun PercentScreen() {
    val viewModel = viewModel(PercentScreenViewModel::class.java)
    val context = LocalContext.current
    val focusManager = LocalFocusManager.current
    val datesArray = context.resources.getStringArray(R.array.date).toList()
    val percentApplicationArray = context.resources.getStringArray(R.array.percent_application_array).toList()
    val taxState by viewModel.taxState.collectAsState()
    Column(modifier = Modifier
        .fillMaxSize()
        .background(Transparent_grey)
        .verticalScroll(rememberScrollState())
        .clickable(MutableInteractionSource(), null) { focusManager.clearFocus() }
        .padding(10.dp)) {
        ComposableWithHeader(header = stringResource(id = R.string.amount)) {
            TextFieldWithSuffix(i = object: TextFieldInterface {
                override fun onTextChanged(amount: Float) {
                    viewModel.setInvestmentAmount(amount.toInt())
                }
            })
        }

        Row(modifier = Modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Max), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
            ComposableWithHeader(header = stringResource(id = R.string.term), modifier = Modifier.weight(3.0f, true)) {
                TextFieldWithSuffix(i = object: TextFieldInterface {
                    override fun onTextChanged(amount: Float) {
                        viewModel.setTermLength(amount.toInt())
                    }
                })
            }
            Column(modifier = Modifier.weight(3.0f, true), verticalArrangement = Arrangement.Bottom) {
                Divider(modifier = Modifier
                    .weight(1.0f, true)
                    .background(Color.Transparent), color = Color.Transparent)
                DropDownMenu(datesArray, modifier = Modifier.weight(1.0f, true), i = object : DropDownMenuInterface {
                    override fun onItemSelected(position: Int) {
                        viewModel.setDateType(position)
                    }
                })
            }
        }
        Row(modifier = Modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Max), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
            ComposableWithHeader(header = stringResource(id = R.string.percent), modifier = Modifier.weight(3.0f, true)) {
                TextFieldWithSuffix(int = false, suffix = "%", i = object: TextFieldInterface {
                    override fun onTextChanged(amount: Float) {
                        viewModel.setPercentAmount(amount)
                    }
                })
            }
            ComposableWithHeader(header = stringResource(id = R.string.percent_application), modifier = Modifier.weight(3.0f, true)) {
                DropDownMenu(percentApplicationArray, i = object : DropDownMenuInterface {
                    override fun onItemSelected(position: Int) {
                        viewModel.setPercentApplicationInterval(position)
                    }
                })
            }
        }
        Row(modifier = Modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Max), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
            ComposableWithHeader(header = stringResource(id = R.string.addition), modifier = Modifier.weight(3.0f, true)) {
                TextFieldWithSuffix(i = object: TextFieldInterface {
                    override fun onTextChanged(amount: Float) {
                        viewModel.setIncrementAmount(amount.toInt())
                    }
                })
            }
            ComposableWithHeader(" ", modifier = Modifier.weight(3.0f, true)) {
                DropDownMenu(percentApplicationArray, i = object : DropDownMenuInterface {
                    override fun onItemSelected(position: Int) {
                        viewModel.setIncrementInterval(position)
                    }
                })
            }
        }
        Row(modifier = Modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Max), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
            ComposableWithHeader(header = stringResource(id = R.string.tax), modifier = Modifier.weight(3.0f, true)) {
                TextFieldWithSuffix(suffix = "%", int = false, i = object: TextFieldInterface {
                    override fun onTextChanged(amount: Float) {
                        viewModel.setTaxAmount(amount)
                    }
                })
            }
            ComposableWithHeader(" ", modifier = Modifier.weight(3.0f, true)) {
                TaxCheckBox(state = taxState, onStateChanged = viewModel::changeTaxState)
            }
        }
        Spacer(modifier = Modifier.weight(1.0f, true))
        GreenButton(text = stringResource(id = R.string.btn_calculate),modifier = Modifier.fillMaxWidth()) {
            viewModel.calculate()
        }
    }

    val resultPopupState by viewModel.resultPopup.collectAsState()
    resultPopupState?.let { CalculatorResultPopup(it) { viewModel.hideResultPopup() }

    }
}