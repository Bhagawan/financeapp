package com.example.financeapp.ui.screens.newsScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.financeapp.ui.composables.NewsPopup
import com.example.financeapp.ui.composables.NewsPreview
import com.example.financeapp.ui.theme.Blue_dark
import com.example.financeapp.ui.theme.Blue_light

@Composable
fun NewsScreen() {
    val viewModel = viewModel(NewsScreenViewModel::class.java)
    val scrollState = rememberLazyListState()
    val news by viewModel.news.collectAsState()
    val popupNews by viewModel.popup.collectAsState()
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        if(popupNews == null) LazyColumn(modifier = Modifier
            .fillMaxHeight(0.9f)
            .fillMaxWidth(0.8f)
            .background(Blue_light, RoundedCornerShape(10.dp))
            .border(width = 1.dp, color = Blue_dark, RoundedCornerShape(10.dp))
            .padding(10.dp),
            state = scrollState,
            verticalArrangement = Arrangement.spacedBy(5.dp),
            horizontalAlignment = Alignment.CenterHorizontally) {
            items(news) { item ->
                NewsPreview(news = item, modifier = Modifier.fillMaxWidth()) {
                    viewModel.showNews(item)
                }
            }
        } else NewsPopup(news = popupNews!!, modifier = Modifier
            .fillMaxHeight(0.9f)
            .fillMaxWidth(0.8f)
            .background(Color.White, RoundedCornerShape(10.dp))
            .border(width = 1.dp, color = Blue_dark, RoundedCornerShape(10.dp))) {
            viewModel.hideNews()
        }
    }
}