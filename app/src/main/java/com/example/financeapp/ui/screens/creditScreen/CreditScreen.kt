package com.example.financeapp.ui.screens.creditScreen

import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.financeapp.R
import com.example.financeapp.ui.composables.*
import com.example.financeapp.ui.theme.Blue_light
import com.example.financeapp.ui.theme.Red
import com.example.financeapp.ui.theme.Transparent_grey
import java.util.*

@Composable
fun CreditScreen() {
    val viewModel = viewModel(CreditScreenViewModel::class.java)
    val context = LocalContext.current
    val focusManager = LocalFocusManager.current
    val percentApplicationArray = context.resources.getStringArray(R.array.percent_application_array).toList()
    val paymentTable by viewModel.paymentsTable.collectAsState()
    val tableScrollState = rememberLazyListState()

    var tableHeight by remember { mutableStateOf(0.dp) }
    val impossiblePaymentPlanNotification = viewModel.cannotPayNotification.collectAsState()

    val mCalendar = Calendar.getInstance()
    mCalendar.time = Date()
    val year = mCalendar.get(Calendar.YEAR)
    val month = mCalendar.get(Calendar.MONTH)
    val day = mCalendar.get(Calendar.DAY_OF_MONTH)

    var mDate by remember { mutableStateOf("$day/${month + 1}/$year") }

    val mDatePickerDialog = DatePickerDialog(
        context,
        { _: DatePicker, mYear: Int, mMonth: Int, mDayOfMonth: Int ->
            viewModel.setStartDate(mYear, mMonth, mDayOfMonth)
            mDate = "$mDayOfMonth/${mMonth+1}/$mYear"
        }, year, month, day
    )

    Column(modifier = Modifier
        .fillMaxSize()
        .background(Transparent_grey)
        .verticalScroll(rememberScrollState())
        .clickable(MutableInteractionSource(), null) { focusManager.clearFocus() }
        .padding(10.dp)) {
        ComposableWithHeader(header = stringResource(id = R.string.credit_amount)) {
            TextFieldWithSuffix(i = object: TextFieldInterface {
                override fun onTextChanged(amount: Float) {
                    viewModel.setCreditAmount(amount.toInt())
                }
            })
        }
        Row(modifier = Modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Max), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
            ComposableWithHeader(header = stringResource(id = R.string.credit_percent), modifier = Modifier.weight(3.0f, true)) {
                TextFieldWithSuffix(int = false, suffix = "%", i = object: TextFieldInterface {
                    override fun onTextChanged(amount: Float) {
                        viewModel.setCreditPercent(amount)
                    }
                })
            }
            ComposableWithHeader(modifier = Modifier.weight(3.0f, true)) {
                DropDownMenu(percentApplicationArray, i = object : DropDownMenuInterface {
                    override fun onItemSelected(position: Int) {
                        viewModel.setPercentIntervalType(position)
                    }
                })
            }
        }

        Row(modifier = Modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Max), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
            ComposableWithHeader(header = stringResource(id = R.string.credit_payment), modifier = Modifier.weight(3.0f, true)) {
                TextFieldWithSuffix(i = object: TextFieldInterface {
                    override fun onTextChanged(amount: Float) {
                        viewModel.setPaymentsAmount(amount.toInt())
                    }
                })
            }
            Column(modifier = Modifier.weight(3.0f, true), verticalArrangement = Arrangement.Bottom) {
                Divider(modifier = Modifier
                    .weight(1.0f, true)
                    .background(Color.Transparent), color = Color.Transparent)
                DropDownMenu(percentApplicationArray, modifier = Modifier.weight(1.0f, true), i = object : DropDownMenuInterface {
                    override fun onItemSelected(position: Int) {
                        viewModel.setPaymentInterval(position)
                    }
                })
            }
        }
        ComposableWithHeader(header = stringResource(id = R.string.credit_date), modifier = Modifier.fillMaxWidth()) {
            Text(mDate, color = Color.Black, textAlign = TextAlign.Center, fontSize = 14.sp,
                modifier = Modifier
                    .background(Blue_light, RoundedCornerShape(10.dp))
                    .padding(5.dp)
                    .padding(horizontal = 20.dp)
                    .align(Alignment.Center)
                    .clickable(MutableInteractionSource(), null) { mDatePickerDialog.show() })
        }

        Box(modifier = Modifier
            .fillMaxWidth()
            .onSizeChanged {
                tableHeight = it.height.dp
            }
            .weight(1.0f, true)
            .padding(vertical = 5.dp)
            .background(Blue_light, RoundedCornerShape(10.dp))
            .clip(RoundedCornerShape(10.dp))
            .border(width = 1.dp, color = Color.White, shape = RoundedCornerShape(10.dp))
            .padding(10.dp)) {
            Column(modifier = Modifier, verticalArrangement = Arrangement.spacedBy(5.dp),
                horizontalAlignment = Alignment.CenterHorizontally) {
                Text(stringResource(id = R.string.credit_table), color = Color.Black, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, fontSize = 15.sp)
                Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                    Text(
                        stringResource(id = R.string.credit_table_date), color = Color.Black, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, fontSize = 10.sp,
                        modifier = Modifier.weight(1.0f, true))
                    Text(stringResource(id = R.string.credit_table_amount), color = Color.Black, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, fontSize = 10.sp,
                        modifier = Modifier.weight(1.0f, true))
                    Text(stringResource(id = R.string.credit_table_added), color = Color.Black, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, fontSize = 10.sp,
                        modifier = Modifier.weight(1.0f, true))
                    Text(stringResource(id = R.string.credit_table_left), color = Color.Black, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, fontSize = 10.sp,
                        modifier = Modifier.weight(1.0f, true))
                }
                LazyColumn(state = tableScrollState, modifier = Modifier
                    .weight(1.0f, true), verticalArrangement = Arrangement.spacedBy(5.dp),
                    horizontalAlignment = Alignment.CenterHorizontally) {
                    itemsIndexed(paymentTable) { index, paymentState ->
                        PaymentStateItem(state = paymentState, grey = index % 2 == 1)
                    }
                }
            }
            if(impossiblePaymentPlanNotification.value) {
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .height(tableHeight)
                    .background(
                        Transparent_grey, RoundedCornerShape(10.dp)
                    ), contentAlignment = Alignment.Center) {
                    Text(stringResource(id = R.string.credit_impossible), fontSize = 20.sp, textAlign = TextAlign.Center, color = Red )
                }
            }
        }

        GreenButton(text = stringResource(id = R.string.btn_calculate),modifier = Modifier.fillMaxWidth()) {
            viewModel.calculate()
        }
    }
}