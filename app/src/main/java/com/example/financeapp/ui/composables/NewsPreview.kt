package com.example.financeapp.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.financeapp.util.News

@Composable
fun NewsPreview(news: News, modifier: Modifier = Modifier, onClick: () -> Unit) {
    Column(modifier = modifier
        .background(Color.White, RoundedCornerShape(10.dp))
        .padding(10.dp)
        .clickable(MutableInteractionSource(),null) { onClick() }, horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
        Image(painter = rememberImagePainter(news.url), contentDescription = news.header, contentScale = ContentScale.Inside, modifier = Modifier.height(100.dp))
        Text(text = news.header, fontSize = 10.sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, color = Color.Black, lineHeight = 14.sp)
    }
}