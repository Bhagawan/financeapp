package com.example.financeapp.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.financeapp.R
import com.example.financeapp.util.News

@Composable
fun NewsPopup(news: News, modifier: Modifier = Modifier, onClose: () -> Unit) {
    val scrollState = rememberScrollState()
    Column(modifier = modifier
        .padding(5.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(5.dp)) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .clickable { onClose() }) {
            Image(painter = painterResource(id = R.drawable.ic_baseline_arrow_back_24), contentDescription = null, modifier = Modifier
                .size(30.dp)
                .align(Alignment.CenterVertically))
        }
        Column(modifier = Modifier.fillMaxWidth().verticalScroll(scrollState), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
            Text(news.header, fontSize = 20.sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, color = Color.Black)
            Image(painter = rememberImagePainter(news.url), contentDescription = news.header, contentScale = ContentScale.Inside, modifier = Modifier.height(150.dp))
            Text(text = news.text, fontSize = 10.sp, textAlign = TextAlign.Center, color = Color.Black)
        }

    }

}