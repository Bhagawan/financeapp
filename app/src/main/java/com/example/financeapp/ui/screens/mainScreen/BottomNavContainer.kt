package com.example.financeapp.ui.screens.mainScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import coil.compose.rememberImagePainter
import com.example.financeapp.ui.screens.creditScreen.CreditScreen
import com.example.financeapp.ui.screens.newsScreen.NewsScreen
import com.example.financeapp.ui.screens.percentScreent.PercentScreen
import com.example.financeapp.util.UrlBack


@Composable
fun BottomNavContainer(
    navController: NavHostController,
    padding: PaddingValues
) {
    Image(rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    NavHost(
        navController = navController,
        startDestination = BottomNavObjects.CALCULATOR.label,
        modifier = Modifier.padding(paddingValues = padding),

        builder = {
            composable(BottomNavObjects.CALCULATOR.label) {
                PercentScreen()
            }
            composable(BottomNavObjects.CREDIT.label) {
                CreditScreen()
            }
            composable(BottomNavObjects.NEWS.label) {
                NewsScreen()
            }
        })
}