package com.example.financeapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Yellow
import androidx.compose.ui.unit.Dp
import coil.compose.rememberImagePainter
import com.example.financeapp.util.UrlLogo

@Composable
fun SplashScreen() {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        Image(rememberImagePainter(UrlLogo), contentDescription = null)
        CircularProgressIndicator( color = Yellow, strokeWidth = Dp(5.0f),
            modifier = Modifier
                .padding(Dp(20.0f))
                .size(Dp(100.0f)))
    }
}