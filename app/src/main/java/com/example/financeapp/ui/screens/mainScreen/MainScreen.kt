package com.example.financeapp.ui.screens.mainScreen

import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.navigation.compose.rememberNavController

@Composable
fun MainScreen() {
    val navController = rememberNavController()
    Scaffold(
        bottomBar = {
            BottomNavBar(navController = navController)
        }, content = { padding ->
            BottomNavContainer(navController = navController, padding = padding)
        }
    )
}