package com.example.financeapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.financeapp.R
import com.example.financeapp.ui.theme.*
import com.example.financeapp.util.CalculationResult

@Composable
fun CalculatorResultPopup(result: CalculationResult, onHide: () -> Unit ) {
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onHide() }, contentAlignment = Alignment.Center) {
        Column(modifier = Modifier
            .fillMaxWidth(0.8f)
            .wrapContentHeight()
            .background(Blue_dark, RoundedCornerShape(10.dp))
            .border(width = 1.dp, color = Blue_light, shape = RoundedCornerShape(10.dp))
            .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
            Text(text = stringResource(id = R.string.invested), fontSize = 15.sp, textAlign = TextAlign.Center, color = Color.White)
            Text(text = result.invested.toString(), fontSize = 20.sp, textAlign = TextAlign.Center, color = Yellow)

            Text(text = stringResource(id = R.string.taxed), fontSize = 15.sp, textAlign = TextAlign.Center, color = Color.White)
            Text(text = result.taxed.toString(), fontSize = 20.sp, textAlign = TextAlign.Center, color = Red)

            Text(text = stringResource(id = R.string.profit), fontSize = 15.sp, textAlign = TextAlign.Center, color = Color.White)
            Text(text = result.profit.toString(), fontSize = 20.sp, textAlign = TextAlign.Center, color = Green)

            GreenButton(text = stringResource(id = R.string.btn_close), modifier = Modifier.height(40.dp)) {
                onHide()
            }
        }
    }
}