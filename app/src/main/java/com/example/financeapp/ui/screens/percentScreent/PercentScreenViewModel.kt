package com.example.financeapp.ui.screens.percentScreent

import androidx.lifecycle.ViewModel
import com.example.financeapp.util.CalculationResult
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlin.math.pow

class PercentScreenViewModel: ViewModel() {
    private var investment: Int = 0
    private var term = 0
    private var dateType = 0
    private var percentAmount = 0.0f
    private var percentApplicationInterval = 0
    private var increment = 0
    private var incrementIntervalType = 0
    private var taxAmount = 0.0f
    private var _taxState = MutableStateFlow(false)
    var taxState = _taxState.asStateFlow()

    private val _resultPopup = MutableStateFlow<CalculationResult?>(null)
    val resultPopup = _resultPopup.asStateFlow()

    fun setInvestmentAmount(amount: Int) {
        investment = amount
    }

    fun setTermLength(term: Int) {
        this.term = term
    }

    fun setDateType(type: Int) {
        dateType = type
    }

    fun setPercentAmount(amount: Float) {
        percentAmount = amount
    }

    fun setPercentApplicationInterval(type: Int) {
        percentApplicationInterval = type
    }

    fun setIncrementAmount(amount: Int) {
        increment = amount
    }

    fun setIncrementInterval(type: Int) {
        incrementIntervalType = type
    }

    fun setTaxAmount(amount: Float) {
        taxAmount = amount
    }

    fun changeTaxState(newState: Boolean) {
        _taxState.tryEmit(newState)
    }

    fun calculate() {
        val capitalisation = when(percentApplicationInterval) {
            0 -> 1.0f
            1 -> 2.0f
            else -> 12.0f
        }
        val dateLength = when(dateType) {
            0 -> term.toFloat()
            else -> term / 12.0f
        }
        val incrementInMonth = when(incrementIntervalType) {
            0 -> increment / 12.0f
            1 -> increment / 6.0f
            else -> increment.toFloat()
        }
        var final = investment * (1 + (percentAmount / 100.0f) / capitalisation).pow(dateLength * capitalisation)
        if(increment > 0) {
            final += (incrementInMonth * ((1 + (percentAmount / 100.0f) / capitalisation).pow(dateLength * capitalisation) - 1)) / ((percentAmount / 100.0f) / capitalisation)
        }
        val invested = investment + increment * (dateLength * when(incrementIntervalType) {
            0 -> 1.0f
            1 -> 2.0f
            else -> 12.0f
        })
        val taxed = if(taxState.value) {
            (final - invested) * (taxAmount / 100.0f) // не совсем верно, нужна точная формула
        } else 0.0f
        showResultPopup(CalculationResult(invested.toInt(), taxed.toInt(), (final - invested - taxed).toInt()))
    }

    private fun showResultPopup(result: CalculationResult) {
        _resultPopup.tryEmit(result)
    }

    fun hideResultPopup() {
        _resultPopup.tryEmit(null)
    }
}