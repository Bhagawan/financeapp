package com.example.financeapp.ui.screens.creditScreen

import androidx.lifecycle.ViewModel
import com.example.financeapp.util.PaymentState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.text.SimpleDateFormat
import java.util.*

class CreditScreenViewModel: ViewModel() {
    private var creditAmount: Int = 0
    private var creditPercent = 0.0f
    private var creditPercentApplicationInterval = 0 // 0 - year, 1 - half Year, 2 - month
    private var paymentAmount: Int = 0
    private var paymentsIntervalType = 0
    private val startDate = Calendar.getInstance()

    private val _paymentsTable = MutableStateFlow<List<PaymentState>>(emptyList())
    val paymentsTable = _paymentsTable.asStateFlow()

    private val _cannotPayNotification = MutableStateFlow(false)
    val cannotPayNotification = _cannotPayNotification.asStateFlow()

    fun setCreditAmount(amount: Int) {
        creditAmount = amount
    }

    fun setCreditPercent(percent: Float) {
        creditPercent = percent
    }

    fun setPercentIntervalType(type: Int) {
        creditPercentApplicationInterval = type
    }

    fun setPaymentsAmount(amount: Int) {
        paymentAmount = amount
    }

    fun setPaymentInterval(type: Int) {
        paymentsIntervalType = type
    }

    fun setStartDate(year: Int, month: Int, dayOfMonth: Int) {
        startDate.set(year,month, dayOfMonth)
    }

    fun calculate() {
        val percentStep = when (creditPercentApplicationInterval) {
            0 -> 12
            1 -> 6
            else -> 1
        }
        val paymentStep = when (paymentsIntervalType) {
            0 -> 12
            1 -> 6
            else -> 1
        }
        _cannotPayNotification.tryEmit(false)
        if(creditPercent / 100.0f * (creditAmount - paymentAmount * (percentStep / paymentStep)) >= paymentAmount * (percentStep / paymentStep)) {
            _cannotPayNotification.tryEmit(true)
            _paymentsTable.tryEmit(emptyList())
        } else if(paymentAmount > 0) {
            val monthStep = kotlin.math.min(paymentStep, percentStep)
            val list = ArrayList<PaymentState>()
            val tempDate = startDate.clone() as Calendar
            val dateFormat = SimpleDateFormat("yyyy/MM/dd", Locale.getDefault())

            var tempAmount = creditAmount
            var tempPaymentStep = paymentStep
            var tempPercentStep = percentStep
            var tempPayment : Int
            var tempPercent : Int
            while(tempAmount > 0) {
                val m = tempDate.get(Calendar.MONTH) //check
                val y = tempDate.get(Calendar.YEAR)
                if(m + monthStep >= 12) tempDate.set(Calendar.YEAR, y + 1)
                tempDate.set(Calendar.MONTH, m + monthStep % 12)

                tempPaymentStep -= monthStep
                tempPercentStep -= monthStep
                if(tempPaymentStep <= 0) {
                    tempPaymentStep = paymentStep
                    tempPayment = paymentAmount.coerceAtMost(tempAmount)
                } else tempPayment = 0
                if(tempPercentStep <= 0) {
                    tempPercentStep = percentStep
                    tempPercent = ((tempAmount - tempPayment) * (creditPercent / 100.0f)).toInt()
                } else tempPercent = 0
                tempAmount = (tempAmount - tempPayment + tempPercent).coerceAtLeast(0)
                list.add(PaymentState(dateFormat.format(tempDate.time), tempPayment, tempPercent, tempAmount))
            }
            _paymentsTable.tryEmit(list.toList())
        }
    }
}