package com.example.financeapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun TextFieldWithSuffix(suffix: String = "", int: Boolean = true, i: TextFieldInterface) {
    var text by remember { mutableStateOf("") }
    val focusManager = LocalFocusManager.current
    BasicTextField(
        value = text,
        onValueChange = {
            try {
                val a = it.toFloat()
                if(a > 0) {
                    text = if(int) it.toInt().toString() else it
                    i.onTextChanged(a)
                }
            } catch (e: Exception) { }
        },
        singleLine = true,
        textStyle = TextStyle(fontSize = 14.sp, color = Color.Black, textAlign = TextAlign.Center),
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
        keyboardActions = KeyboardActions(onDone = {
            focusManager.clearFocus()
        }),
        decorationBox = { innerTextField ->
            Row(
                modifier = Modifier
                    .background(color = Color.White, RoundedCornerShape(7.dp))
                    .padding(1.dp).clipToBounds(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                    innerTextField()
                    Text(suffix, color = Color.Black, fontSize = 14.sp, modifier = Modifier.align(Alignment.CenterEnd))
                }
            }
        }
    )
}

interface TextFieldInterface {
    fun onTextChanged(amount: Float)
}