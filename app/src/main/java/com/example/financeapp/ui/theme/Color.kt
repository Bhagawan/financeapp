package com.example.financeapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val Blue_dark = Color(0xFF253642)
val Blue_light = Color(0xFF6C8CA5)
val Yellow = Color(0xFFA77800)
val Red = Color(0xFFB6483A)
val Green = Color(0xFF5BA761)
val Grey_light = Color(0xFF949494)
val Transparent_grey = Color(0x80000000)