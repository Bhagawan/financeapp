package com.example.financeapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.financeapp.ui.theme.Blue_light

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun DropDownMenu(items: List<String>, modifier: Modifier = Modifier, i: DropDownMenuInterface ) {
    var expanded by remember { mutableStateOf(false) }
    var selectedItem by remember { mutableStateOf(items[0]) }
    ExposedDropdownMenuBox(expanded = expanded, onExpandedChange = { expanded = !expanded }, modifier = modifier
        .background(color = Blue_light, RoundedCornerShape(5.dp))
        .padding(1.dp)) {
        BasicTextField(
            value = selectedItem,
            textStyle = TextStyle(fontSize = 14.sp),
            onValueChange = {},
            readOnly = true,
            enabled = false,
            decorationBox = { innerTextField ->
                Box(modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                     innerTextField()
                    Box(modifier = Modifier
                        .align(Alignment.CenterEnd)
                        .size(20.dp)) {
                        ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded)
                    }
                    if(expanded) Divider(Modifier.height(1.dp).align(Alignment.BottomCenter))
                }
            }
        )
        ExposedDropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.padding(0.dp).background(Blue_light)
        ) {
            items.forEachIndexed { index, item ->
                Column(
                    Modifier
                        .fillMaxWidth()
                        .clipToBounds()
                        .clickable {
                            selectedItem = item
                            expanded = false
                            i.onItemSelected(index)
                        }) {
                    if(index > 0) Divider(Modifier.height(1.dp))
                    Text(text = item, fontSize = 14.sp, color = Color.Black, textAlign = TextAlign.Left, modifier = Modifier.align(Alignment.CenterHorizontally))
                }
            }
        }
    }
}

interface DropDownMenuInterface {
    fun onItemSelected(position: Int)
}