package com.example.financeapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.financeapp.ui.theme.Grey_light
import com.example.financeapp.util.PaymentState

@Composable
fun PaymentStateItem(state: PaymentState, grey: Boolean = false, modifier: Modifier = Modifier) {
    val color = if(grey) Grey_light else Color.White
    Row(modifier = modifier.background(color).padding(5.dp), verticalAlignment = Alignment.CenterVertically) {
        Text(state.date, color = Color.Black, textAlign = TextAlign.Center, fontSize = 10.sp,
            modifier = Modifier.weight(1.0f, true))
        Text(state.amount.toString(), color = Color.Black, textAlign = TextAlign.Center, fontSize = 10.sp,
            modifier = Modifier.weight(1.0f, true))
        Text(state.addedPercent.toString(), color = Color.Black, textAlign = TextAlign.Center, fontSize = 10.sp,
            modifier = Modifier.weight(1.0f, true))
        Text(state.left.toString(), color = Color.Black, textAlign = TextAlign.Center, fontSize = 10.sp,
            modifier = Modifier.weight(1.0f, true))
    }
}