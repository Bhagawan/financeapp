package com.example.financeapp.ui.screens.mainScreen

import com.example.financeapp.R

enum class BottomNavObjects(val label: String, val iconId: Int) {
    CALCULATOR("% Calc.", R.drawable.ic_baseline_calculate_24),
    CREDIT("Credit", R.drawable.ic_baseline_calculate_24),
    NEWS("News", R.drawable.ic_baseline_newspaper_24)
}