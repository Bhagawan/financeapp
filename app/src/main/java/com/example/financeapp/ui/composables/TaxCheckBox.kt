package com.example.financeapp.ui.composables

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.financeapp.ui.theme.Blue_light
import com.example.financeapp.ui.theme.Yellow

@Composable
fun TaxCheckBox(state: Boolean, onStateChanged: (Boolean) -> Unit) {
    Checkbox(
        checked = state,
        modifier = Modifier.padding(16.dp),
        onCheckedChange = { onStateChanged(it) },
        colors = CheckboxDefaults.colors(uncheckedColor = Blue_light, checkedColor = Yellow, checkmarkColor = Color.White)
    )
}