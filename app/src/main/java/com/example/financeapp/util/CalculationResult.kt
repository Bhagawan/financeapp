package com.example.financeapp.util

data class CalculationResult(val invested: Int, val taxed: Int, val profit: Int)
