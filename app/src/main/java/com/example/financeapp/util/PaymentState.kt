package com.example.financeapp.util

data class PaymentState(val date: String, val amount: Int, val addedPercent: Int, val left: Int)
