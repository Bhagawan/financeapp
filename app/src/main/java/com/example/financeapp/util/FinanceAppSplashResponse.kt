package com.example.financeapp.util

import androidx.annotation.Keep

@Keep
data class FinanceAppSplashResponse(val url : String)