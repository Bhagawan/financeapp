package com.example.financeapp.util

import androidx.annotation.Keep

@Keep
data class News(val url: String, val header: String, val text: String)
